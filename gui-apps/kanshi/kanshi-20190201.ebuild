# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CRATES="
bitflags-1.0.4
byteorder-1.2.7
cfg-if-0.1.6
edid-0.2.0
getopts-0.2.18
i3ipc-0.8.4
itoa-0.4.3
libc-0.2.43
libudev-0.2.0
libudev-sys-0.1.4
log-0.3.9
log-0.4.6
memchr-1.0.2
nom-3.2.1
pkg-config-0.3.14
ryu-0.2.7
serde-1.0.80
serde_json-1.0.33
unicode-width-0.1.5
xml-rs-0.7.0
xmltree-0.8.0
"

inherit cargo

GIT_HASH="970267e400c21a6bb51a1c80a0aadfd1e6660a7b"

DESCRIPTION="Dynamic display manager"
HOMEPAGE="https://github.com/emersion/kanshi"
SRC_URI="http://github.com/emersion/kanshi/archive/${GIT_HASH}.tar.gz -> ${P}.tar.gz"

S="${WORKDIR}/${PN}-${GIT_HASH}"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
