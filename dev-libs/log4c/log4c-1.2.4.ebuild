# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="Logging FrameWork for C, as Log4j or Log4Cpp"
HOMEPAGE="http://log4c.sourceforge.net/"
SRC_URI="http://prdownloads.sourceforge.net/log4c/log4c-1.2.4.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""
