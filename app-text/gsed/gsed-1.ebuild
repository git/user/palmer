# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="GNU sed wrapper"
HOMEPAGE=""
SRC_URI=""

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="sys-apps/sed"
BDEPEND=""

src_unpack() {
	mkdir -p ${S}
}

src_install() {
	mkdir -p ${ED}/usr/bin
	ln -s /bin/sed ${ED}/usr/bin/gsed
}
