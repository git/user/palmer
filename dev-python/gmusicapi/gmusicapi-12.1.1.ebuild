# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 python3_6 )
inherit distutils-r1

DESCRIPTION="An unofficial client library for Google Music."
HOMEPAGE="https://unofficial-google-music-api.readthedocs.io/en/latest/"
SRC_URI="https://github.com/simon-weber/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	dev-python/appdirs
	dev-python/decorator
	dev-python/future
	dev-python/gpsoauth
	dev-python/httplib2
	dev-python/MechanicalSoup
	dev-python/mock
	media-libs/mutagen
	dev-python/oauth2client
	dev-python/pbr
	dev-python/proboscis
	dev-python/protobuf-python
	dev-python/pyasn1
	dev-python/pyasn1-modules
	dev-python/pycryptodomex
	dev-python/python-dateutil
	dev-python/requests
	dev-python/rsa
	dev-python/six
	dev-python/validictory
"
BDEPEND=""
