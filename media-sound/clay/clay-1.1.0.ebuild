# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_6 )
inherit distutils-r1

DESCRIPTION="Awesome standalone command line player for Google Play Music."
HOMEPAGE="https://github.com/and3rson/clay"
SRC_URI="https://github.com/and3rson/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	dev-python/gmusicapi
	dev-python/pyyaml
	dev-python/urwid
	dev-python/codename
	media-video/vlc
	dev-libs/keybinder
"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${P}-unsafe_load.patch"
)
